const Joi = require('joi');
const express = require('express');
const app = express();

app.use(express.json());
const router = express.Router();

const courses = [

    {'id':1,'name':'Data Structutre'},
    {'id':2,'name':'Data Structutre 1'},
    {'id':3,'name':'Data Structutre 2'},
]
app.get('/', function(req, res){
    res.send("Hello World!");
 });

 app.get('/api/courses', function(req, res){
    //res.send([1,2,3]);  //Stattic Code
    res.send(courses);
 });
 app.get('/api/courses/:id', function(req, res){
    //res.send(req.params.id);
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if(!course) res.status(404).send('This course with the given Id was not found');
    res.send(course);
 });
 app.post('/api/courses', function(req, res){
    //res.send(req.params.id);

    //Validation with Joi library
 
    //const result = ValidateCourse(req.body);

    const { error } = ValidateCourse(req.body); // == result.error

    if(error){
        res.status(400).send(error.details[0].message);
        return;
    }
    
    //console.log("hiii"+req.body.name.length); 
    // if(!req.body.name || req.body.name.length < 3){
    //     // 404 Bad request
    //     res.status(400).send('name required more than min 3 charatcter');
    //     return;
    // }
    const course = {
        id : courses.length + 1,
        name : req.body.name
    };
    courses.push(course);
    res.send(course);
 });

 app.put('/api/courses', function(req, res){
     //Look up the course 
     // Exist or not
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if(!course) res.status(404).send('This course with the given Id was not found');

    //Validation with Joi library 
    //const result = ValidateCourse(req.body);

    const { error } = ValidateCourse(req.body); // == result.error

    if(error){
        res.status(400).send(error.details[0].message);
        return;
    }

    course.name = req.body.name;
    res.send(course);
  
 });

 app.delete('/api/courses/:id', function(req, res){
     //Look up the course 
     // Exist or not
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if(!course) res.status(404).send('This course with the given Id was not found');

    //Delete

    const index = courses.indexOf(course);
    courses.splice(index,1);
    res.send(course);
	
 });

 function ValidateCourse(course){
    const schema = {
        name : Joi.string().min(3).required()
    };
    return Joi.validate(course,schema);
 }


 // http://localhost:3000/api/post/1991/12
 app.get('/api/post/:year/:month', function(req, res){
    res.send(req.params);
 });
 
// http://localhost:3000/api/post/1991/12?sortBy=sfbuds
 app.get('/api/post/:year/:month', function(req, res){
    res.send(req.query);
 });

 app.post('/hello', function(req, res){
    res.send("You just called the post method at '/hello'!\n");
 });
 app.all('/test', function(req, res){
    res.send("HTTP method doesn't have any effect on this route!");
 });

 const port = process.env.port || 3000;

app.listen(port,()=>console.log('Listing on port 3000.....'));